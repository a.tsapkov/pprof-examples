package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"runtime"
	"time"
)

type Post struct {
	ID       int
	Text     string
	Author   string
	Comments int
	Time     time.Time
}

func handle(w http.ResponseWriter, _ *http.Request) {
	result := ""
	for i := 0; i < 100; i++ {
		currPost := &Post{ID: i, Text: "new post", Time: time.Now()}
		jsonRaw, _ := json.Marshal(currPost)
		result += string(jsonRaw)
	}
	time.Sleep(3 * time.Millisecond)
	w.Write([]byte(result))
}

func main() {
	runtime.GOMAXPROCS(4)
	http.HandleFunc("/", handle)

	const port = "8080"
	fmt.Printf("starting server at %s\n", port)
	fmt.Println(http.ListenAndServe(":"+port, nil))
}
