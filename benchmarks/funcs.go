package benchmarks

func FastFillSlice(size int) {
	s := make([]int, 0, size)
	for i := 0; i < size; i++ {
		s = append(s, i)
	}
}

func SlowFillSlice(size int) {
	s := make([]int, 0)
	for i := 0; i < size; i++ {
		s = append(s, i)
	}
}
