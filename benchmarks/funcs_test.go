package benchmarks

import "testing"

func BenchmarkFastFillSlice(b *testing.B) {
	for i := 0; i < b.N; i++ {
		FastFillSlice(100000)
	}
}

func BenchmarkSlowFillSlice(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SlowFillSlice(100000)
	}
}
